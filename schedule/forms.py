from django import forms
from .models import Schedule

class ScheduleForms(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = ["namaKegiatan", "kategori", "lokasi", "tanggal", "waktu"]