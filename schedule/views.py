from django.shortcuts import render
from django.views import generic
from .models import Schedule
from .forms import ScheduleForms
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.edit import DeleteView

def sched_list(request):
    schedules = Schedule.objects.order_by('tanggal')
    return render(request, 'sched_list.html', {'schedules':schedules})

def new_schedule(request):
    if request.method == "POST":
        form = ScheduleForms(request.POST)
        if form.is_valid():
            schedule = form.save()
            schedule.save()
            return redirect('/schedules/')
    else:   
        form = ScheduleForms()
    return render(request, 'addschedule.html', {'form': form})

class delete_schedule(DeleteView):
    model = Schedule
    success_url = reverse_lazy('schedule:sched_list')