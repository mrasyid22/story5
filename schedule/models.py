from django.db import models
from django.conf import settings
from django.utils import timezone

class Schedule(models.Model):
    namaKegiatan = models.CharField(max_length=25)
    kategori = models.CharField(max_length=25)
    lokasi = models.CharField(max_length=25)
    tanggal = models.DateField()
    waktu = models.TimeField()

    def __str__(self):
        return self.namaKegiatan  

    def getTanggal(self):
        return self.tanggal.strftime("%d")

    def getBulan(self):
        return self.tanggal.strftime("%b")

    def getHari(self):
        return self.tanggal.strftime("%A")

    
    

    