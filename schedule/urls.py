from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'schedule'

urlpatterns = [
    path('', views.sched_list, name='sched_list'),
    path('new/', views.new_schedule, name='new_schedule'),
    url(r'(?P<pk>[0-9]+)/delete/$', views.delete_schedule.as_view(), name='delete_schedule'),
]
