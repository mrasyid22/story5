from django.urls import path, include
from django.contrib import admin

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', include('homepage.urls')),
    path('', include('schedule.urls')),
]